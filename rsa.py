import binascii
from Crypto.PublicKey import RSA
import noise_rng as rng
from Crypto.Cipher import AES, PKCS1_OAEP
import socket
import random as rand
import os
from subprocess import check_output
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256

def getNoiseSample(duration, driver, waiting=5):
   URL = 'http://websdr.ewi.utwente.nl:8901/'
   driver.get(URL)
   time.sleep(waiting)
   driver.execute_script("soundapplet.audioresume()")
   driver.execute_script("record_click()")
   time.sleep(duration)
   driver.execute_script("record_click()")
   element=driver.find_element(By.ID,"reccontrol").find_element(By.TAG_NAME,'a')
   element.click()
   time.sleep(1)#sometimes it can not finish download

rngAudioSourceName = 0
generator = 0
private_key = 0
public_key = 0
fileToEncrypt = 0
while 1:
   print()
   print('0 - Convert .wav to .m4a')
   print('1 - Create new generator')
   if generator: print('2 - Generate new key pair')
   if public_key: print('3 - Print public key')
   if private_key: print('4 - Print private key')
   if public_key: print('5 - Encrypt file')
   if fileToEncrypt: print('6 - Decrypt file here')
   print('7 - send file locally')
   if private_key: print('8 - damage key')
   print('9 - download noise sample')
   if private_key: print('10 - Sign file')
   print('11 - exit')
   print()
   option = input("Choose option: ")

   os.system('clear')
   if option == '0':
      wavfile = input("Specify .wav file path: ")
      m4afile = input("Specify .m4a converted file path: ")
      check_output(f"ffmpeg -i {wavfile} -c:a aac -q:a 2 {m4afile}", shell=True)
   elif option == '1':
      rngAudioSourceName = input("Input path file(.m4a) for RNG source: ")
      print(f"Chosen file path: {rngAudioSourceName}")
      print("Random number generation in process...")
      try:
         generator = rng.RNG(rngAudioSourceName) 
      except:
         print("Couldn't find file")
      else:
         print("Ready to generate keys!")
         generator.histPercent()
   elif option == '2' and generator:
      try:
         key = RSA.generate(2048, generator.getNextRandom)
         private_key = key.export_key()
         file_out = open("private.pem", "wb")
         file_out.write(private_key)
         file_out.close()
         public_key = key.publickey().export_key()
         file_out = open("receiver.pem", "wb")
         file_out.write(public_key)
         file_out.read
         file_out.close()
      except:
         print("Something went wrong!")
      else:
         print("New key pair generated!")
   elif option == '3' and public_key:
      with open("receiver.pem") as f:
         key = f.read()
         print(key)
  
   elif option == '4' and private_key:
      with open("private.pem") as f:
         key = f.read()
         print(key)
   elif option == '5':
      fileToEncrypt = input("Input file name you want to encrypt: ")
      fileData = 0
      try:
         with open(fileToEncrypt, 'rb') as f:
            fileData = f.read()
      except:
         print("Couldn't find file")
      else:
         file_out = open(f"{fileToEncrypt}_encrypted.bin", "wb")

         recipient_key = RSA.import_key(open("receiver.pem").read())
         session_key = generator.getNextRandom(16)

         cipher_rsa = PKCS1_OAEP.new(recipient_key)
         enc_session_key = cipher_rsa.encrypt(session_key)

         cipher_aes = AES.new(session_key, AES.MODE_EAX)
         ciphertext, tag = cipher_aes.encrypt_and_digest(fileData)
         [ file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext) ]
         file_out.close()
   elif option == '6' and fileToEncrypt and private_key:
      try:
         private_key = RSA.import_key(open('private.pem').read())
         file_in = open(f"{fileToEncrypt}_encrypted.bin", "rb")
         enc_session_key, nonce, tag, ciphertext = \
               [ file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1) ]
         file_in.close()
      except:
         print(f"Wrong key, otherwise check if file path: {fileToEncrypt}_encrypted.bin is valid")
      else:
         cipher_rsa = PKCS1_OAEP.new(private_key)
         session_key = cipher_rsa.decrypt(enc_session_key)

         cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
         data = cipher_aes.decrypt_and_verify(ciphertext, tag)
         input('Click enter to continue')
         saveName = input("Save decrypted file as: ")
         file_out = open(f"{saveName}", "wb")
         file_out.write(data)
         file_out.close()
         print(f"File saved as {saveName}")
   elif option == '7':

      fileToSend = input("Specify file to send: ")

      if os.path.exists(fileToSend):
         myLocalIp = input("Specify your local ip address: ")
         port = input("Specify port: ")


         s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
         s.bind((myLocalIp, int(port)))

         print("Waiting for client...")
         s.listen(10)
         c, addr = s.accept()
         print('{} connected.'.format(addr))

         f = open(f"{fileToSend}", "rb")
         datas = f.read(1024)

         while datas:
            c.send(datas)
            datas = f.read(1024)

         f.close()
         print("Done sending...")
         c.close()
         s.close()
      else: print("Couldn't find specified file!")
   elif option == '8':
      key = 0
      with open("private.pem") as f:
         key = f.read()
      key = list(key)
      rand.shuffle(key)
      key = ''.join(key)
      key = bytes(key,'utf-8')
      file_out = open("private.pem", "wb")
      file_out.write(key)
      file_out.close()
   elif option == '9':
      duration = input("Specify duration of recorded sound[s]: ")
      try:
         duration = int(duration)
      except:
         print("It's not a number!")
      else:

         print("1. Chrome")
         print("2. Firefox")
         
         browserDriver = input('Pick your browser: ')
         
         if browserDriver == '1':
            with webdriver.Chrome() as driver:
               getNoiseSample(duration, driver)
         elif browserDriver == '2':
            with webdriver.Firefox() as driver:
               getNoiseSample(duration, driver)
         else: print("Picked none.")
   
   elif option == '10':
      fileName = input("Input file name you want to sign: ")
      with open(fileName, 'rb') as fileToSign:
         with open('private.pem', 'rb') as pKey:
            hash = SHA256.new(fileToSign.read())
            signature = pkcs1_15.new(RSA.import_key(pKey.read())).sign(hash)
            open('sign', 'wb').write(binascii.hexlify(signature))
   elif option == '11': break