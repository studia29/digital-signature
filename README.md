# Digital Signature

Made with Python version 3.9.12

What you can do using this project?

- Encrypt and decrypt files using RSA.
- Send files through local network.
- Test encryption.

## Getting started

1. Refer to "What is required" in order to run the project.
2. Move decrypt.py to client machine.
3. You might want to disable your firewalls if needed to send files.

## What is required:

Python libs(with recommended versions):

- matplotlib==3.5.2
- numpy==1.22.4
- pycryptodome==3.14.1
- pydub==0.25.1
- selenium==4.2.0

Other:

- installed ffmpeg and system variable path specifed for ffmpeg

- Chrome or Firefox browser and compatible with browser version webdriver(side tool option, not needed for overall functionality)
  Chrome webdriver: https://chromedriver.chromium.org/downloads
  Firefox webdriver: https://github.com/mozilla/geckodriver/releases

Extract webdriver .exe to project destination.

## Description

- noise_rng.py - Uses audio file(.m4a) in order to generate random numbers. Audio should be some kind of noise to get good RNG source
- rsa.py - Server/sender side menu
- decrypt.py - Client/receiver side menu
- sources - example noise samples are in there

## Installation

You can use 'pip install' to install required libs speciied in 'What is required'
Refer to https://ffmpeg.org/ to install ffmpeg

## Usage

Note: When specifying file paths, you should also give their file format

rsa.py:

If you need to obtain noise sample you can use University of Twente WebSDR http://websdr.ewi.utwente.nl:8901/ or other WebSDR websites

You can record and download sample selecting different frequencies there.

There is also an option to download sample automatically with Firefox or Chrome browser(9), it will download to your default download directory.

Convert downloaded .wav to .m4a from menu(0)

1. It's required to create new RNG from .m4a file first(1). You can do it using e.g. './sources/89.2.m4a' path
2. To continue close displayed histogram, then you need to generate new pair of keys(2)
3. It will be possible to print these keys(3,4), encrypt any file you want(5) e.g. some text file. And you can damage private key automatically now(8).
4. Encrypted file will be saved with previous file name + \_encrypted.bin
5. After encryption you can decrypt file on the same machine(6)
6. You can send file through your local network anytime(7)
7. After generation of key pair you will be able to sign file(10)

decrypt.py:

1. Download file from local network(1), to do so specify local ip address it can be 'localhost' or ip of the server. Then specify the port which is used by server.
2. File will be saved in destination you used to launch the program.
3. Load file you want to decrypt and obtained(from local network, flash drive etc.) private key(2)
4. Decrypt specified file(3)
5. If you have 'sign' file(sender must give it) and public key, you can check if file you recived is valid(4)

Sending file through local network:

1. Choose option 7 from rsa.py menu.
2. Specify information you are asked for on server machine.
3. After 'Waiting for client...' appears, choose option 1 on your client machine from launched decrypt.py
4. Specify information you are asked for on your client machine.
5. If everything went smooth and provided info was right, then on both server and client you should be able to choose options from menu again.

# Author

Kamil Dezor
