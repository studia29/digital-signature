import numpy as np
import matplotlib.pyplot as plt
from pydub import AudioSegment
import random as rand

class RNG:
    def histPercent(self):
        setData = set(self.random)

        dictOfWords = { i : 0 for i in setData }

        for x in self.random:
            dictOfWords[x]+=1

        dictOfWords2 = { i : (dictOfWords.get(i)/len(self.random)) for i in setData }

        plt.plot(dictOfWords2.keys(), dictOfWords2.values())
        plt.title("Probability")
        plt.show()

    def randomFromM4A(self):
        a = AudioSegment.from_file(self.fileName,format='m4a')
        sig= a.get_array_of_samples()

        sig_int8 = np.uint8(sig)

        without_zero= [a for a in sig_int8 if a > 1]

        return without_zero

    def getNextRandom(self, N):

        randString = b''
        while(len(randString)<N):
            self.iterator+=1
            if(self.iterator==len(self.random)):
                print("RNG source utilized, it's recommended to create new generator or use bigger source!")
                self.iterator = 0
            randString += self.random[self.iterator]
        
        return randString

    def __init__(self, fileName):
        self.fileName = fileName
        self.iterator = -1
        self.random = self.randomFromM4A()
        rand.shuffle(self.random)