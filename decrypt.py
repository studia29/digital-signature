import binascii
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
import socket
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA256

keyPath = 0
fileName = 0
while 1:
    print('0 - exit')
    print('1 - download file')
    print('2 - Load private key and file to decrypt')
    if keyPath: print('3 - Decrypt file')
    if keyPath: print('4 - Check if file was changed')
    option = input("Choose option: ")

    if option == '0':
        break
    elif option == '1':
        saveAs = input("How to name obtained file?: ")
        senderIp = input("Specify sender ip address(LAN): ")
        port = input("Specify port sender uses: ")

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((senderIp, int(port)))

        f = open(f"{saveAs}", "wb")
        print("Downloading...")
        while True:
            datas = s.recv(1024)
            while datas:
                f.write(datas)
                datas = s.recv(1024)
            f.close()
            break
        s.close()
        print("Done receiving")
    elif option == '2':
        keyPath = input("Specify path to key: ")
        fileName = input("Specify encrypted file path: ")
    elif option == '3' and keyPath and fileName:

        try:
            file_in = open(fileName, "rb")
            private_key = RSA.import_key(open(keyPath).read())
            enc_session_key, nonce, tag, ciphertext = \
                        [ file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1) ]
            file_in.close()
        except:
            print("Wrong file or key path")
        else:
                cipher_rsa = PKCS1_OAEP.new(private_key)
                session_key = cipher_rsa.decrypt(enc_session_key)

                cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
                data = cipher_aes.decrypt_and_verify(ciphertext, tag)

                saveName = input("Save decrypted file as: ")
                file_out = open(f"{saveName}", "wb")
                file_out.write(data)
                file_out.close()
                print(f"File saved as {saveName}")
    elif option == '4':
      fileName = input("Input file name you want to check: ")
      with open(fileName, 'rb') as fileToCheck:
         with open('receiver.pem', 'rb') as pKey:
            with open('sign', 'rb') as signature:
                key = RSA.import_key(pKey.read())
                hash = SHA256.new(fileToCheck.read())
                try:
                    pkcs1_15.new(key).verify(hash, binascii.unhexlify(signature.read()))
                    print("The signature is valid.")
                except (ValueError, TypeError):
                    print("The signature is not valid.")
            